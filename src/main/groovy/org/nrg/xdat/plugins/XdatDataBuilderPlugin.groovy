/*
 * xdat-data-builder: org.nrg.xdat.plugins.XdatDataBuilderPlugin
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xdat.plugins

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.tasks.bundling.Jar
import org.gradle.api.tasks.compile.JavaCompile
import org.nrg.xdat.tasks.XdatDataBuilderBeanJarTask
import org.nrg.xdat.tasks.XdatDataBuilderGenerateSourceTask

class XdatDataBuilderPlugin implements Plugin<Project> {
    void apply(Project project) {
        // Set the primary compileJava task to depend on generate source.
        (project.tasks.getByName("compileJava") as JavaCompile).with {
            dependsOn project.task('xdatDataBuilder', type: XdatDataBuilderGenerateSourceTask)
        }

        // Set the primary jar task to trigger bean jar task.
        project.task('xdatBeanJar', type: XdatDataBuilderBeanJarTask).dependsOn project.tasks.getByName("jar")
    }
}
