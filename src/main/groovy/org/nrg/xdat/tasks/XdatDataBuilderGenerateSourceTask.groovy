/*
 * xdat-data-builder: org.nrg.xdat.tasks.XdatDataBuilderGenerateSourceTask
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xdat.tasks

import org.apache.commons.io.FileUtils
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.bundling.Jar
import org.gradle.api.tasks.compile.JavaCompile
import org.nrg.xft.generators.GenerateResources

import java.nio.file.Path
import java.nio.file.Paths

class XdatDataBuilderGenerateSourceTask extends DefaultTask {
    def Path generated
    def File java
    def File resources

    public XdatDataBuilderGenerateSourceTask() {
        // Set group and description
        group = "xnat"
        description = "Generates Java, Velocity, and other resources from XNAT datatype-definition schemas."

        // Everything will be generated under this folder.
        generated = Paths.get project.buildDir.absolutePath, "xnat-generated"
        java = generated.resolve("src/main/java").toFile()
        resources = generated.resolve("src/main/resources").toFile()

        (project.tasks.getByName("compileJava") as JavaCompile).with {
            source java
        }
    }

    @TaskAction
    def process() {
        // Get the root project path.
        def root = Paths.get(project.projectDir.absolutePath)

        // Where schema are located
        def schemaSources = root.resolve("src/main/resources/schemas").toFile()
        if (!schemaSources.exists()) {
            logger.warn "No schemas found to generate resources for"
        } else {
            // Delete generated resources if they exist
            if (generated.toFile().exists()) {
                generated.toFile().deleteDir()
            }

            // Each type of content is generated into a particular location.
            def resourcePath = Paths.get(resources.toURI())
            def schemas = resourcePath.resolve("schemas").toFile()
            def templates = resourcePath.resolve("META-INF/resources/base-templates/screens").toFile()
            def javascript = resourcePath.resolve("META-INF/resources/scripts").toFile()

            //copy schemas to destination (including display docs if they are there)
            FileUtils.copyDirectory(schemaSources, schemas, new FileFilter() {
                @Override
                boolean accept(final File pathname) {
                    return pathname.directory || pathname.name =~ /^.*\.(xml|xsd)/
                }
            })

            // Where customized classes are defined (like customized Base*.java)
            def javaSources = Paths.get(project.projectDir.absolutePath, "src/main/java").toFile()

            // Generate the resources in the destination
            new GenerateResources(project.name, schemas.getAbsolutePath(), java.absolutePath, templates.absolutePath, javaSources.absolutePath, java.absolutePath, resources.absolutePath, javascript.absolutePath).process();
        }
    }
}
