/*
 * xdat-data-builder: org.nrg.xdat.tasks.XdatDataBuilderBeanJarTask
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xdat.tasks

import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.bundling.Jar

class XdatDataBuilderBeanJarTask extends Jar {
    XdatDataBuilderBeanJarTask() {
        // Set group and description
        group = "xnat"
        description = "Creates the beans jar from classes in the org.nrg.xdat.bean and org.nrg.xdat.model packages."

        // Set basename for this to include "-beans"
        baseName = "${project.name}-beans"

        // Set a specific source set to include just bean and model classes.
        def mainSourceSet = ((SourceSetContainer) project.properties.get("sourceSets")).getByName("main")
        from mainSourceSet.output
        includes = ["org/nrg/xdat/bean/**/*", "org/nrg/xdat/model/**/*"]
    }

    @TaskAction
    def process() {
        // Nothing to do in here, everything is done at configuration.
        logger.lifecycle("Creating beans jar ${baseName}")
    }
}
