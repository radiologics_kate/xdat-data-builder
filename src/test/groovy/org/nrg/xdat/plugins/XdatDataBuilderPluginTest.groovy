/*
 * xdat-data-builder: org.nrg.xdat.plugins.XdatDataBuilderPluginTest
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xdat.plugins

import org.junit.Test
import org.nrg.xdat.common.XdatDataBuilderTest
import org.nrg.xdat.tasks.XdatDataBuilderGenerateSourceTask

import static org.junit.Assert.assertTrue

class XdatDataBuilderPluginTest extends XdatDataBuilderTest {
    @Test
    public void xdatPluginAddsXdatDataBuilderTaskToProject() {
        assertTrue(project.tasks.xdatDataBuilder instanceof XdatDataBuilderGenerateSourceTask)
    }
}
