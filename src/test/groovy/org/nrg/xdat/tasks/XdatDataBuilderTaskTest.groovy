/*
 * xdat-data-builder: org.nrg.xdat.tasks.XdatDataBuilderTaskTest
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xdat.tasks

import org.junit.Test
import org.nrg.xdat.common.XdatDataBuilderTest

import static org.junit.Assert.*

class XdatDataBuilderTaskTest extends XdatDataBuilderTest {
    XdatDataBuilderTaskTest() {
        super(false)
    }

    @Test
    public void canAddTaskToProject() {
        def task = project.task('xdatDataBuilder', type: XdatDataBuilderGenerateSourceTask)
        assertTrue(task instanceof XdatDataBuilderGenerateSourceTask)
    }
}
