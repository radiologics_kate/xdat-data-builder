/*
 * xdat-data-builder: org.nrg.xdat.common.XdatDataBuilderTest
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xdat.common

import org.gradle.api.plugins.JavaPlugin
import org.gradle.testfixtures.ProjectBuilder
import org.nrg.xdat.plugins.XdatDataBuilderPlugin

class XdatDataBuilderTest {
    def project

    XdatDataBuilderTest(boolean includePlugin = true) {
        project = ProjectBuilder.builder().build()
        project.pluginManager.apply JavaPlugin
        if (includePlugin) {
            project.pluginManager.apply XdatDataBuilderPlugin
        }
    }
}
